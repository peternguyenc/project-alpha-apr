from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from .forms import LoginForm, SignUpForm
from django.contrib.auth.forms import User


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LoginForm()
    return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password1"]

            # Create a new user account using create_user
            user = User.objects.create_user(
                username=username, password=password
            )

            # Log in the user using Django's login function
            login(request, user)

            # Redirect to the 'list_projects' page upon successful signup
            return redirect(
                "home"
            )  # Assuming 'projects:home' is the name of your project list view
    else:
        form = SignUpForm()
    return render(request, "registration/signup.html", {"form": form})
