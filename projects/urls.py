from django.urls import path
from . import views

app_name = "projects"


urlpatterns = [
    path("", views.list_projects, name="home"),
    path("<int:id>/", views.show_project, name="show_project"),
]
