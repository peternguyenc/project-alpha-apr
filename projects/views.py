from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from .models import Project


@login_required
def list_projects(request):
    projects = Project.objects.filter(members=request.user)
    return render(request, "project_list.html", {"projects": projects})


@login_required  # Ensure that only logged-in users can access this view
def show_project(request, id):
    # Retrieve the project based on the provided id
    project = get_object_or_404(Project, id=id)

    # Render the project details template with the project object
    return render(
        request, "projects/project_details.html", {"project": project}
    )
